import PropTypes from 'prop-types';

import Card from "./components/Card";
import './AllCards.scss'

function AllCards({handlerCurrentProd, handlerModal, data, handlerFavorite, text, checkbox}){
    const mappedData = data.map((item, index) => <Card checkbox={checkbox} text={text} handlerFavorite={handlerFavorite} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} key={index} data={item} />);
    const cl = checkbox ? 'grids' : 'cards';
    const wrGr = checkbox ? 'wrap_grids' : 'wrap';
    return(
        
            <div className={wrGr}>
                <div className={cl} >
                    {data && mappedData}
                </div>
            </div>
        
       
    )
}
AllCards.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    data: PropTypes.array,
};
export default AllCards;