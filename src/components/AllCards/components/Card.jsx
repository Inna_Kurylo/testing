import PropTypes from 'prop-types';
import Button from '../../Button'; 
import {ReactComponent as Favorite} from './icons/favorites.svg';
import {ReactComponent as FavActive} from './icons/favActive.svg';
import ButtonClose from '../../Button/components/ButtonClose.jsx'

function Card({data, handlerFavorite, handlerModal, handlerCurrentProd, text, checkbox}){
    const {
        name,
        price,
        img,
        articul,
        color,
        inFav,
        inCart
    } = data;
    const className = "icon-favoriteCards";
    const viewCard = checkbox ? "grids__card" : "cards__card";
    const viewImg = checkbox ? "grids__img" : "card__img";
    const viewTemp = checkbox ? "grids__temp" : "card__temp";
    return(
        <>
         {!inCart ?
            (<div className={viewCard}  >
            <span className={className} onClick={(event) => {handlerFavorite(data); event.stopPropagation();}}>
                {inFav ? <FavActive  /> : <Favorite/>}
            </span>
                <p className="card__data">{name} <br/> {price}</p>
            <div className="card__icon">
                <img className={viewImg} src={img} alt={articul}/>
            </div>
             <span className={viewTemp}>{color}</span>
            <Button checkbox={checkbox} text={text} handlerClick={()=> {
                handlerModal();
                handlerCurrentProd(data)
                }}/>
        </div>)
            : 
                        (<div className={"cards__card cards__card--white"}  >
                            <ButtonClose classWhite = {'close-white'} click ={() => {
                                 handlerModal();
                                 handlerCurrentProd(data)}}/>
                            <p className="card__data">{name} <br/> {price}</p>
                            <div className="card__icon">
                                <img className="card__img" src={img} alt={articul}/>
                            </div>
                            <span className="card__temp">{color}</span>
          
                        </div>)
        }
    
         </>
        )
     
    }

Card.propTypes = {
	name: PropTypes.string,
    price: PropTypes.number,
    img: PropTypes.string,
    color: PropTypes.string,
    inFav: PropTypes.bool,
};
export default Card;