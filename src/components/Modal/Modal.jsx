import {useEffect, useRef } from "react";
import PropTypes from 'prop-types'; 
import ButtonClose from "../Button/components/ButtonClose";
import "./Modal.scss"

function Modal({title, action, closeModal, textModal}){

	const wrapperRef = useRef();

	useEffect(() =>{
		document.addEventListener("mousedown", handleClickOutside);

		return(() => {
			document.removeEventListener("mousedown", handleClickOutside);
		})
	})

	const handleClickOutside = (event) => {
		if (wrapperRef && ! wrapperRef.current.contains(event.target)) { 
			closeModal();
		}
	}
	return (
		<div data-testid='modal' className="modal-wrapper">
			<div className="modal" ref={wrapperRef}>
				<div className="modal-box">
					<ButtonClose click={closeModal}/>
					<div className="modal-header">
						<h4>{textModal}</h4>
					</div>
					<div className="modal-content">
						{title}
					</div>
					<div className="modal-footer">
						{action}
					</div>
				</div>
			</div>
		</div>
	)

}
Modal.propTypes = {
    closeModal: PropTypes.func,
    action: PropTypes.element,
	title: PropTypes.string,
} 

export default Modal;