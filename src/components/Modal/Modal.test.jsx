import Modal from "./Modal";
import {render} from '@testing-library/react';
describe("Modal",()=>{
    it("testing modal", ()=>{
        const {container} = render(<Modal/>);
        expect(container).toBeInTheDocument();
    })
})