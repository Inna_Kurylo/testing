import { render,fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { BrowserRouter } from "react-router-dom";
import CheckboxProvider from '../../context';

import App from '../../App'
import store from '../../store';
import {actionCards} from '../../reducers/cards.reducer';

describe('Cards testing',()=>{
    it('button click', ()=>{

        const mock = [{"name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
            "price":"41 499 ₴",
            "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
            "articul":"352486503",
            "color":"Red"}];
        store.dispatch(actionCards(mock));
        const {container,getByTestId} =  render(<Provider store={store}><CheckboxProvider><BrowserRouter>
            <App />
          </BrowserRouter></CheckboxProvider></Provider>); 

        let button = container.querySelector('button');
        fireEvent.click(button);
        let wrap = container.querySelector('.modal-wrapper');
        expect(wrap).toBeInTheDocument();
        
        button = container.querySelector('.close-delete');
        fireEvent.click(button);
        //wrap = container.querySelector('.modal-wrapper');
        expect(wrap).not.toBeInTheDocument();

    }
    )
})