import PropTypes from 'prop-types';
import {ReactComponent as Cart} from './components/icons/cart.svg';
import './Button.scss';


function Button({handlerClick, text,checkbox}){
    const classes = checkbox ? 'button_grid' : 'button';
    const classesText = checkbox ? 'button_grid--text' : 'button--text';
    return(
        <div className="wrapper">
                <button data-testid="click" id = "button" className ={classes} type="button" onClick={handlerClick}>
                    <span className= {classesText}>
                    {text}
                    </span>
                    <Cart/>
                </button>
            </div>
    )
}

Button.propTypes = {
	handlerClick: PropTypes.func,
};
export default Button;