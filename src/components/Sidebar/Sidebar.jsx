import { Link, NavLink } from "react-router-dom";
import { ReactComponent as Favorites } from "./icons/favorites.svg";
import { ReactComponent as Logo } from "./icons/logo.svg";
import { ReactComponent as Cart } from "./icons/cart.svg";

import "./Sidebar.scss";

function Sidebar({count, countFav}) {
	return (
		<>
		<div className="header__logo">
				<Link to="/"  className="logo">
					<Logo /> <span className="logo--text">Sunny</span>
				</Link>
		</div>
		<div className="nav">
		<div className="header__favorites-list">
				<span className="icon-favorite">
					< NavLink to="/" activeClassName="active" className="nav-link ">
						 Home
					</NavLink>
				</span>
			</div>
			<div className="header__favorites-list">
				<span className="icon-favorite">
					<span className="count count--cart">{count}</span>
					< NavLink to="/cart" activeClassName="active" className="nav-link ">
						<Cart /> Cart
					</NavLink>
				</span>
			</div>
			<div className="header__favorites-list">
					<span className="icon-favorite">
						<span className="countFav">{countFav}</span>
						<NavLink to="/favorites" activeClassName="active" className="nav-link ">
							<Favorites /> Favorites
						</NavLink>
					</span>
			</div>
			</div>


		</>)
}
export default Sidebar;
