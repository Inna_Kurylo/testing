import {Field, ErrorMessage} from "formik";
import cx from "classnames";
import PropTypes from "prop-types";

import "./Input.scss";

const Input = ({className, error, label, type, inputName, placeholder, ...restProps}) =>{
    return(
        <label className={cx("form-item", className,{'has-validation':error})}>
        <p className="form-label">{label}</p>
        <Field type={type} 
            className="form-control" 
            name={inputName} 
            placeholder={placeholder} 
            {...restProps}/>
         <ErrorMessage className="error-message" name={inputName} component={"p"}/> 
    </label>
    )
};
Input.defaultProps = {
    type: "text",
} 
Input.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
    inputName:  PropTypes.string,
    label: PropTypes.string,
    className: PropTypes.string,
    error: PropTypes.object,
}

export default Input;