import { render } from "@testing-library/react";
import Input from "./Input";
import {Formik} from "formik";

describe ('Input Snapshot', ()=>{
    it('Input', ()=>{
        const input = render(<Formik><Input inputName='name'/></Formik>)
        expect(input).toMatchSnapshot()
    
    });
    
})