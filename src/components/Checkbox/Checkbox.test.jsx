import { render, screen } from '@testing-library/react';
import Checkbox from './Checkbox';

describe('Snapshot Checkbox', ()=>{

    test('Checkbox', ()=>{
        const checkbox = render(<Checkbox/>)

        expect(checkbox).toMatchSnapshot()
    })

})