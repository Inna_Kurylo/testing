import React from 'react';
import './Checkbox.scss';


const Checkbox = ({onClick, checkbox}) => {
    //console.log(checkbox);
	return (
		<label className="g-checkbox">
			<input type="checkbox"  onClick={onClick}
									checked={checkbox}
									onChange={()=>{}}/>
			Table view
		</label>
	);
};

export default Checkbox;
