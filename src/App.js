import { useEffect } from 'react';
import { Route, Routes } from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import Header from './components/Header';
import { Modal } from './components/Modal';
import Footer from './components/Footer/Footer';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Favorite from './pages/Favorite';
import { actionModal, actionCurrentProd, actionAddFavorites, actionRemoveFavorite,actionAddCart, actionRemoveCart } from './reducers';
import { selectorCards, selectorAppModal, selectorAppFavorites, selectorAppCart,selectorCurrentProd } from './selectors';
import { actionFetchCards } from './reducers';

function App() {
  const cards = useSelector(selectorCards);
  const isModal = useSelector(selectorAppModal);
  const currentProd = useSelector(selectorCurrentProd);
  const favorite = useSelector(selectorAppFavorites);
  const cart = useSelector(selectorAppCart);
  
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('hjhkhjkhjh');
      dispatch(actionFetchCards());
  }, [dispatch]);

  const handlerModal = () => {
    dispatch(actionModal(!isModal))
  };

 const handlerCurrentProd = (currentProd) => {
    dispatch(actionCurrentProd(currentProd));
  }

  const handlerFavorite = (heart) => {
    const isAdd = favorite.some(row => row.articul === heart.articul);
    if (isAdd) {
      dispatch(actionRemoveFavorite( heart));
     } else {
      dispatch(actionAddFavorites(heart));
     }
   
  }

  const handlerDeleteCart = (del) => {
    dispatch(actionRemoveCart( del));

  }

  const handlerCart = (currentProd) => {
    const isAddCart = cart.some(row => row.articul === currentProd.articul);
    if (!isAddCart) {
      dispatch(actionAddCart(currentProd));
      handlerModal();
    } else handlerModal();
  }
  const action = (
    <div className="button-wrapper">
      <button className="btn" type="button" onClick={ 
      !currentProd?.inCart ? () => handlerCart(currentProd) : () => {
        handlerModal()
        handlerDeleteCart(currentProd)
      }
}
       
      >OK</button>
      <button className="btn" type="button" onClick={() => handlerModal()}>Cancel</button>
    </div>)

  const cardUpDate = cards.map(row => {
    return {
      ...row,
      inFav: favorite.some(dataCard => dataCard.articul === row.articul),
    };
  });
  const textBuy = 'Buy';
  const textToCart = 'To cart';

  const cartUpDate = cart.map(row => {
    return {
      ...row,
      inCart: true,
    };
  });
  let textModal = isModal && currentProd.inCart ? "Do you want delete this product from the cart?" : "Do you want to add a product to your cart?"
  return (
    <div className="page__wrapper">
      <Header count={cart.length} countFav={favorite.length} />
      <main className="main">
        <Routes>
          <Route path="/" element={<Home text={textToCart} cards={cardUpDate} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} handlerFavorite={handlerFavorite} />} />
          <Route path="/favorites" element={<Favorite text={textToCart} favorite={favorite} handlerFavorite={handlerFavorite} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal}/>} />
          <Route path="/cart" element={<Cart cart={cartUpDate} text={textBuy} handlerModal={handlerModal} handlerCurrentProd={handlerCurrentProd} />} />
        </Routes>
      </main>
      <Footer />
      {isModal && <Modal  textModal={textModal} action={action} closeModal={handlerModal} title={currentProd.name} />}

    </div>
  )
}
export default App;