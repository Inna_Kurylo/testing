import { createContext, useState } from 'react';

export const CheckboxContext = createContext();

const CheckboxProvider = ({children}) =>{
    const [checkbox, setCheckbox] = useState (false);
    const handlerCheckbox = () => setCheckbox(!checkbox);

    return(
        <CheckboxContext.Provider value={{checkbox,handlerCheckbox}}>
            {children}
        </CheckboxContext.Provider>
    )
}

export default CheckboxProvider;