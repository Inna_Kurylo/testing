import appReducer, {actionBuyAll, actionAddCart, actionRemoveCart, actionCurrentProd, actionModal, actionAddFavorites, actionRemoveFavorite} from "./app.reducer";

describe('appSlice', () =>{

    it('should return default state',  ()=>{
        const result = appReducer(undefined, { type: ''});
        const mockState = {
            "cart": [],
            "currentProd":{},
            "favorites": [],
            "modal": false,
        } 
        expect(result).toEqual(mockState);
    });
    it ('should add favorites with "actionAddFavorites" action', ()=>{
       
        const mockState = {};
        mockState.favorites = [];
        
        const action = { type: actionAddFavorites.type, 
                        payload: {"name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
                        "price":"41 499 ₴",
                        "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
                        "articul":"352486503",
                        "color":"Red"}
                        };
        const res = appReducer(mockState,action);

        expect(res.favorites).toEqual([action.payload]);
    });
    it ('should add carts with "actionAddCart" action', ()=>{
        const mockState = {
            cart: []
        };
      /*   mockState.cart = []; */
        
        const action = { type: actionAddCart.type, 
                        payload: {"name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
                        "price":"41 499 ₴",
                        "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
                        "articul":"352486503",
                        "color":"Red"}
                        };
        const res = appReducer(mockState,action);

        expect(res.cart).toEqual([action.payload]);
    });
    it ('should current product with "actionCurrentProd" action', ()=>{
        const mockState = {};
        mockState.currentProd = {};

        const action = {
            type: actionCurrentProd.type, 
            payload: {"name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
                    "price":"41 499 ₴",
                    "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
                    "articul":"352486503",
                    "color":"Red"}
        

        };

        const res = appReducer(mockState, action);
        const {payload} = action;
        expect(res.currentProd).toEqual(payload);
    });
    it('should open the modal with "actionModal" action', ()=>{
        const mockState = {};
        mockState.modal = false;

        const action = {
            type: actionModal.type,
            payload: true
        }
        const res = appReducer(mockState,action)
        expect(res.modal).toBe(true);
    });
    it('should remove from favorites with "actionRemoveFavorite" action', () => {
        const mockState = {};

        mockState.favorites= [{
            "name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
            "price":"41 499 ₴",
            "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
            "articul":"352486503",
            "color":"Red"
        }]

        const action = {
            type: actionRemoveFavorite.type,
            payload:{ "articul":"352486503"}
        }
        const res = appReducer (mockState, action);
        expect(res.favorites).toEqual([])
    });
    it('should remove from cart whith "actionRemoveCar" action', ()=>{
        const mockState = {};
        mockState.cart= [{
            "name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
            "price":"41 499 ₴",
            "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
            "articul":"352486503",
            "color":"Red"
        }];
        const action = { type: actionRemoveCart.type,
                        payload:{ "articul":"352486503"}
                    }
        const res = appReducer(mockState, action)
        expect(res.cart).toEqual([]);
    });
    it('should remove all from cart',()=>{
        const mockState = {};
        mockState.cart= [{
            "name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
            "price":"41 499 ₴",
            "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
            "articul":"352486503",
            "color":"Red"
        }];
        const action = { type: actionBuyAll.type}
        const res = appReducer(mockState, action);
        expect(res.cart).toEqual([])
    })
})
