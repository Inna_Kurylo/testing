import { render } from '@testing-library/react';

import store from '../store';
import {actionCards} from './cards.reducer';

describe('Cards add',()=>{
    it('testing action "actionCards"',()=>{
        const mock = [{"name":"Мобильный телефон Apple iPhone 14 128GB PRODUCT Red (MPVA3RX/A)",
            "price":"41 499 ₴",
            "img":"https://content2.rozetka.com.ua/goods/images/big_tile/284913542.jpg",
            "articul":"352486503",
            "color":"Red"}];
        const res = store.dispatch(actionCards(mock));
       expect(res.payload).toEqual(mock);

    })
})