import cvReducer, {actionCv} from "./cv.reducer";


describe('cvSlice',()=>{
    it('should creat obj with "actionCv" action',()=>{
        const mockState = {};
    mockState.cv = {};
    const action = { type: actionCv.type,
                    payload: {name: "Li",
                    soname: "Chan",
                    phone: "098",
                    address: "street",
                    email: "i@i.i",}
                };
    const res = cvReducer (mockState, action);
    const { payload } = action;
    expect(res.cv).toEqual(payload);
    })
    
})