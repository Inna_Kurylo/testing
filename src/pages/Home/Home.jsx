import PropTypes from 'prop-types';
import { useContext } from 'react';

import AllCards from "../../components/AllCards";
import {CheckboxContext} from '../../context'; 
import Checkbox from '../../components/Checkbox';

function Home({cards, handlerCurrentProd, handlerModal, handlerFavorite, text}) {
    const {checkbox, handlerCheckbox} = useContext(CheckboxContext); 
    return(
    <div className="main">
            <Checkbox onClick={handlerCheckbox} checkbox={checkbox}/>
            <AllCards text={text} data={cards} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} handlerFavorite={handlerFavorite} checkbox={checkbox}/>
           
    </div>)
}
Home.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    text: PropTypes.string,
    cards: PropTypes.array
};
export default Home;