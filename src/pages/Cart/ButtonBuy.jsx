import PropTypes from 'prop-types';
import {ReactComponent as Cart} from '../../components/Button/components/icons/cart.svg';
import '../../components/Button/Button.scss';

function ButtonBuy({checkout, type}){
    return(
        <div className="wrapper">
                <button className ="button" type={type} onClick={checkout}>
                    <span className="button--text">
                    Buy all
                    </span>
                    <Cart/>
                </button>
            </div>
    )
}

ButtonBuy.propTypes = {
	chekout: PropTypes.func,
};
export default ButtonBuy;