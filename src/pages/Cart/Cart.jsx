import PropTypes from 'prop-types';
import {Formik, Form} from "formik";
import { useDispatch, useSelector } from 'react-redux';

import {selectorCv} from "../../selectors";
import {actionCv, actionBuyAll} from '../../reducers';

import { validationSchema } from './validation';
import Input from "../../components/Form/Input";
import Card from "../../components/AllCards/components/Card";
import ButtonBuy from "./ButtonBuy";
import './Cart.scss';


function Cart({handlerCurrentProd, handlerModal, cart, text}){
    const cartPages = cart.map((item, index) => <Card 
                                                handlerCurrentProd={handlerCurrentProd} 
                                                handlerModal={handlerModal}   
                                                key={index} data={item} />);
    const cvData = useSelector(selectorCv);
    const dispatch = useDispatch();
    return(
        <div className="wrap">
                <div className="cards" >
                    {cartPages}
                </div>
                <div className="page">
                    <Formik
                    initialValues = {cvData}
                    onSubmit = {(values) =>{
                        dispatch(actionCv(values));
                        dispatch(actionBuyAll());
                    }}
                    validationSchema = {validationSchema}
                    >
                        {({errors, touched})=>(
                            <Form>
                                <fieldset className="form-block">
                                <legend>User block</legend>
                                <div className="row col-6">
                                <Input className='mb-3' inputName={'name'} label={'Name'} placeholder='name' error={errors.name && touched.name} />
                                <Input className={'mb-3'} inputName={'secondName'} label={'Second name'} placeholder='second name' error={errors.secondName && touched.secondName}/>
                                <Input className={'mb-3'} inputName={'age'} label={'Age'} placeholder='age' error={errors.age && touched.age}/>
                                <Input className={'mb-3'} inputName={'address'} label={'Address:'} placeholder='address' error={errors.address && touched.address}/>
                                <Input className={'mb-3'} inputName={'phone'} label={'Phone'} placeholder='phone' error={errors.phone && touched.phone}/>
                                </div>
                                </fieldset>
                                <ButtonBuy type="submit" text = {text} ></ButtonBuy>
                            </Form>
                        )}
                    </Formik>
                </div>
        </div>
    )
}
Cart.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    text: PropTypes.string,
    cart: PropTypes.array
};
export default Cart;